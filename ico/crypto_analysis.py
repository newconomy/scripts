from bs4 import BeautifulSoup
from copy import deepcopy
from datetime import datetime
from datetime import timedelta
from datetime import timezone
from sklearn import linear_model
from tqdm import tqdm

import csv
import numpy as np
import math
import json
import pandas as pd
import requests
import time

# Получает информацию о всех криптовалютах с главной страницы
def get_quickdata():
    
    try:
        with open('log.txt', 'a') as f:
            f.write('\n\n\n' + str(datetime.now()) + ': =========START=========' + '\n')
    except:
        with open('log.txt', 'w') as f:
            f.write('\n\n\n' + str(datetime.now()) + ': =========START=========' + '\n')
    
    cat_data = pd.read_csv('categories.csv')
    
    url = 'https://coinmarketcap.com/all/views/all/'
    r = requests.get(url)
    soup = BeautifulSoup(r.text, 'lxml')

    headers = soup.find('table', 'summary-table').find_all('tr')[0].text.split('\n')[2:-2]
    table = []

    for el in soup.find('table', 'summary-table').find_all('tr'):
        values = [td.text.strip() for td in el.find_all('td')[1:-1]]
        line = {}
        for h, v in zip(headers, values):
            line[h] = v
            if h == 'Name':
                line[h] = line[h].split('\n')[-1]
            if h == 'Circulating Supply':
                line[h] = line[h].split('\n')[0]
        
        if line != {}:
            try:
                line['Category'] = list(cat_data[cat_data['Name'] == line['Name']]['Category'])[0]
            except:
                pass
            table.append(line)        
            
    df = pd.DataFrame(table)
    headers.append('Category')
    df = df[headers]
    df.to_csv('quick_data.csv', index=False)
    
    try:
        with open('log.txt', 'a') as f:
            f.write(str(datetime.now()) + ': Main page parsed' + '\n')
    except:
        with open('log.txt', 'w') as f:
            f.write(str(datetime.now()) + ': Main page parsed' + '\n')
    
# Получает исторические данные о криптовалютах
# В текущей версии выполняется ~2.5 часа, время простоя при выгрузке страниц можно пробовать уменьшать на свой страх и риск (могут забанить)
# Можно переписать скрипт с использованием API, тогда время выполнения точно уменьшится
def get_icohistory():
    wait_time = 2

    mounths = {
        'Dec': '12',
        'Nov': '11',
        'Oct': '10',
        'Sep': '09',
        'Aug': '08',
        'Jul': '07',
        'Jun': '06',
        'May': '05',
        'Apr': '04',
        'Mar': '03',
        'Feb': '02',
        'Jan': '01',
    }
    
    cat_data = pd.read_csv('categories.csv')
    
    url = 'https://coinmarketcap.com/all/views/all/'
    r = requests.get(url)
    soup = BeautifulSoup(r.text, 'lxml')
    links = ['https://coinmarketcap.com' + el.find('a')['href'] for el in soup.find_all('td', 'currency-name')]
    ico_info = []
    
    #error_links = []
    print('Getting ico history:')
    for link in tqdm(links):
        
        time.sleep(wait_time)
        
        history_start = (datetime.today()-timedelta(days=30)).strftime('%Y%m%d')
        history_end = (datetime.today()+timedelta(days=1)).strftime('%Y%m%d')
        
        url = link + 'historical-data/?start=' + history_start + '&end=' + history_end
        r = requests.get(url)
        soup = BeautifulSoup(r.text, 'lxml')

        try:
            name = soup.find('h1').text.strip().split('\n')[0]
            theader = soup.find('div', 'table-responsive').find('thead').text.split('\n')[2:-2]
            
            try:
                with open('log.txt', 'a') as f:
                    f.write(str(datetime.now()) + ': Parsing history data: ' + name + '\n')
            except:
                with open('log.txt', 'w') as f:
                    f.write(str(datetime.now()) + ': Parsing history data: ' + name + '\n')

            for el in soup.find('div', 'table-responsive').find('tbody').find_all('tr'):
                cur_info = {}
                for col_name, info in zip(theader, el.text.split('\n')[1:-1]):
                    cur_info[col_name.split('*')[0]] = info
                cur_info['name'] = name
                
                try:
                    cur_info['Category'] = list(cat_data[cat_data['Name'] == cur_info['name']]['Category'])[0]
                except:
                    pass
                
                spl_date = cur_info['Date'].replace(',', '').split()
                cur_info['Date'] = spl_date[1] + '.' + mounths[spl_date[0]] + '.' + spl_date[2]
                cur_info['timestamp'] = int(datetime.strptime(cur_info['Date'], \
                                                 "%d.%m.%Y").replace(tzinfo=timezone.utc).timestamp())
                
                ico_info.append(cur_info)
        except:
            pass
            #print('error')
            #error_links.append(link)
            
    df = pd.DataFrame(ico_info)

    df = df[['name', 'Date', 'timestamp', 'Open', 'High', 'Low', 'Close', 'Volume', 'Market Cap', 'Category']]
    df.to_csv('crypto_history.csv', index=False)
    
    try:
        with open('log.txt', 'a') as f:
            f.write(str(datetime.now()) + ': Stop parsing history data' + '\n')
    except:
        with open('log.txt', 'w') as f:
            f.write(str(datetime.now()) + ': Stop parsing history data' + '\n')

# Аналитика
def analysis():
    with open('categories.json') as f:
        cats = json.loads(f.read())
        
    try:
        with open('log.txt', 'a') as f:
            f.write(str(datetime.now()) + ': Start analytics' + '\n')
    except:
        with open('log.txt', 'w') as f:
            f.write(str(datetime.now()) + ': Start analytics' + '\n')
    
    categories = [cat for cat in cats]
    quick_data = pd.read_csv('quick_data.csv')
    
    # Подсчёт аналитики по данным с главной страницы: суммарная капиталицаия и среднее изменение по категориям
    cat_data = {}
    for cat in categories:
        cat_data[cat] = {
            'Cryptos': [],
            'Market Cap': 0,
            'Change Sum': 0,
            'Change Quant': 0
        }

    for i in range(len(quick_data)):
        cur_cat = quick_data.loc[i, 'Category']
        for cat in categories:
            if isinstance(cur_cat, str):
                if cat in cur_cat:
                    cat_data[cat]['Cryptos'].append(quick_data.loc[i, 'Name'])
                    if '?' not in quick_data.loc[i, 'Market Cap']:
                        cat_data[cat]['Market Cap'] += int(quick_data.loc[i, 'Market Cap'].replace(',', '').replace('$', ''))
                    if '?' not in quick_data.loc[i, '% 24h']:
                        cat_data[cat]['Change Sum'] += float(quick_data.loc[i, '% 24h'].replace('%', ''))
                        cat_data[cat]['Change Quant'] += 1
    
    new_cat = []
    for cat in cat_data:
        el = {}
        el['Category'] = cat
        el['Cryptos'] = ', '.join(cat_data[cat]['Cryptos'])
        el['Market Cap, $'] = cat_data[cat]['Market Cap']
        if (cat_data[cat]['Change Quant']) != 0:
            el['Average Change (24h), %'] = round(cat_data[cat]['Change Sum'] / cat_data[cat]['Change Quant'], 2)
        else:
            el['Average Change (24h), %'] = 0
        new_cat.append(el)
        
    analysis = pd.DataFrame(new_cat)
    
    try:
        with open('log.txt', 'a') as f:
            f.write(str(datetime.now()) + ': Quick analytics done' + '\n')
    except:
        with open('log.txt', 'w') as f:
            f.write(str(datetime.now()) + ': Quick analytics done' + '\n')

    # Составление данных для предсказания капитализации
    crypto_history = pd.read_csv('crypto_history.csv')
    
    mounth_back = int(time.mktime((datetime.fromtimestamp(max(crypto_history['timestamp'])) - timedelta(days=30)).timetuple()))
    
    # Обрезаю историю на месяц
    crypto_history_reduced = crypto_history[crypto_history['timestamp'] >= mounth_back]
    crypto_history_reduced = crypto_history_reduced.reset_index()
    
    # Составляю словарь, в котором каждая категория будет содержать валюты из неё
    cryptos = {}
    for i in range(len(analysis)):
        cat = analysis.loc[i, 'Category']
        values = analysis.loc[i, 'Cryptos'].split(', ')
        cryptos[cat] = {}
        for val in values:
            cryptos[cat][val] = {}
        
    # Набор дат, в которые есть данные для каждой из категорий
    timestamp_values = {}
    for i in range(len(crypto_history_reduced)):
        if isinstance(crypto_history_reduced.loc[i, 'Category'], str):
            categories = crypto_history_reduced.loc[i, 'Category'].split(';')
            timestamp = crypto_history_reduced.loc[i, 'timestamp']
            mc = crypto_history_reduced.loc[i, 'Market Cap'].replace(',', '')
            for cat in categories:
                if cat not in timestamp_values:
                    timestamp_values[cat] = set()
                timestamp_values[cat].add(timestamp)
    
    # Для каждой валюты загоняю дни, которые есть в данной категории
    for cat in timestamp_values:
        for ts in timestamp_values[cat]:
            for val in cryptos[cat]:
                cryptos[cat][val][ts] = 0
                
    # Заполняю значения mc для каждой валюты в каждый день
    for i in range(len(crypto_history_reduced)):
        if isinstance(crypto_history_reduced.loc[i, 'Category'], str):
            categories = crypto_history_reduced.loc[i, 'Category'].split(';')
            timestamp = crypto_history_reduced.loc[i, 'timestamp']
            mc = crypto_history_reduced.loc[i, 'Market Cap'].replace(',', '')
            name = crypto_history_reduced.loc[i, 'name']
            for cat in categories:
                if '-' not in mc:
                    if name in cryptos[cat]:
                        cryptos[cat][name][timestamp] = int(mc)
    
    # Преобразование для того, чтобы вставлять пропущенные значения
    cryptos_v2 = {}
    for cat in cryptos:
        cryptos_v2[cat] = {}
        for name in cryptos[cat]:
            cryptos_v2[cat][name] = []
            for ts in cryptos[cat][name]:
                cryptos_v2[cat][name].append([ts, cryptos[cat][name][ts]])
                
    # Словарик с заменами капитализации
    quick_mc = {}
    for i in range(len(quick_data)):
        name = quick_data.loc[i, 'Name']
        value = quick_data.loc[i, 'Market Cap'].replace(',', '').replace('$', '')
        quick_mc[name] = value
    
    # Заменить нулевые валюты
    cryptos_v3 = {}

    for cat in cryptos_v2:
        for name in cryptos_v2[cat]:
            name_sum = 0
            for el in cryptos_v2[cat][name]:
                name_sum += el[1]
            if name_sum != 0:
                if cat not in cryptos_v3:
                    cryptos_v3[cat] = {}
                cryptos_v3[cat][name] = deepcopy(cryptos_v2[cat][name])
            else:
                try:
                    cryptos_v2[cat][name][0][1] = int(quick_mc[name])
                    cryptos_v3[cat][name] = deepcopy(cryptos_v2[cat][name])
                except:
                    pass
    
    # Заполнить пропущенные
    for cat in cryptos_v3:
        for name in cryptos_v3[cat]:
            for i in range(len(cryptos_v3[cat][name])):
                if cryptos_v3[cat][name][i][1] == 0:
                    for j in range(1, len(cryptos_v3[cat][name])):
                        if (i - j) >= 0:
                            if cryptos_v3[cat][name][i - j][1] != 0:
                                cryptos_v3[cat][name][i][1] = cryptos_v3[cat][name][i - j][1]
                                break
                        if (i + j) < len(cryptos_v3[cat][name]):
                            if cryptos_v3[cat][name][i + j][1] != 0:
                                cryptos_v3[cat][name][i][1] = cryptos_v3[cat][name][i + j][1]
                                break
                                
    # Просуммировать по датам
    cryptos_sum = {}
    for cat in cryptos_v3:
        if cat not in cryptos_sum:
                cryptos_sum[cat] = {}
        for name in cryptos_v3[cat]:
            for el in cryptos_v3[cat][name]:
                if el[0] not in cryptos_sum[cat]:
                    cryptos_sum[cat][el[0]] = 0
                cryptos_sum[cat][el[0]] += el[1]
    
    # Преобразования
    cryptos_sum_v2 = []
    for cat in cryptos_sum:
        for ts in cryptos_sum[cat]:
            cryptos_sum_v2.append({'category': cat, 'timestamp': ts, 'market_cap_sum': cryptos_sum[cat][ts]})
    trands_df = pd.DataFrame(cryptos_sum_v2)
    
    try:
        with open('log.txt', 'a') as f:
            f.write(str(datetime.now()) + ': Regression data is ready' + '\n')
    except:
        with open('log.txt', 'w') as f:
            f.write(str(datetime.now()) + ': Regression data is ready' + '\n')
    
    # Предсказание значений по категориям
    cats = list(trands_df['category'].unique())
    plus_week = int(time.mktime((datetime.fromtimestamp(max(trands_df['timestamp'])) + timedelta(days=7)).timetuple()))
    plus_two_week = int(time.mktime((datetime.fromtimestamp(max(trands_df['timestamp'])) + timedelta(days=14)).timetuple()))
    
    cats_regr = {}
    for cat in cats:
        cur_df = trands_df[trands_df['category'] == cat]
        x = list(cur_df['timestamp'])
        y = list(cur_df['market_cap_sum'])

        regr = linear_model.LinearRegression()
        regr.fit(np.array(x).reshape(-1, 1), np.array(y))

        p7, p14 = regr.predict(np.array([plus_week, plus_two_week]).reshape(-1, 1))
        cats_regr[cat] = {
            'Plus 7 days Cap': int(p7), 
            'Plus 14 days Cap': int(p14), 
        }
        
    predicted = pd.DataFrame(cats_regr).T
    
    analysis['Plus 7 days Cap, $'] = ''
    analysis['Plus 14 days Cap, $'] = ''
    
    for i in range(len(analysis)):
        cat = analysis.loc[i, 'Category']
        if cat in cats_regr:
            analysis.loc[i, 'Plus 7 days Cap, $'] = cats_regr[cat]['Plus 7 days Cap']
            analysis.loc[i, 'Plus 14 days Cap, $'] = cats_regr[cat]['Plus 14 days Cap']
    
    analysis = analysis[['Category', 'Cryptos', 'Market Cap, $', 'Average Change (24h), %', \
                                   'Plus 7 days Cap, $', 'Plus 14 days Cap, $']]
    
    analysis.to_csv('result.csv', index=False)

    try:
        with open('log.txt', 'a') as f:
            f.write(str(datetime.now()) + ': =========END=========' + '\n\n\n')
    except:
        with open('log.txt', 'w') as f:
            f.write(str(datetime.now()) + ': =========END=========' + '\n\n\n')
